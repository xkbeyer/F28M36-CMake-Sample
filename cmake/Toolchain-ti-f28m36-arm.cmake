
# This one is important
SET(CMAKE_SYSTEM_NAME Generic)
# This one not so much
SET(CMAKE_SYSTEM_VERSION 1)
SET(CMAKE_TRY_COMPILE_TARGET_TYPE STATIC_LIBRARY)
message(STATUS "${XXX}")
if("$ENV{CG_TOOLS}" STREQUAL "")
    message(FATAL_ERROR "Missing CG_TOOLS environment variable.")
else()
    string(REPLACE "\\" "/" CG_TOOLS $ENV{CG_TOOLS})
    message(STATUS "CG_TOOLS are in: ${CG_TOOLS}")
endif()
if(WIN32)
    set(_exe ".exe")
endif()
# specify the cross compiler
SET(CMAKE_C_COMPILER   ${CG_TOOLS}/bin/armcl${_exe})
SET(CMAKE_CXX_COMPILER ${CMAKE_C_COMPILER})

# where is the target environment
SET(CMAKE_FIND_ROOT_PATH ${CG_TOOLS})

# search for programs in the build host directories
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
# for libraries and headers in the target directories
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

SET(CMAKE_INCLUDE_PATH "${CG_TOOLS}/include ${CG_TOOLS}/include/libcxx")
SET(CMAKE_LIBRARY_PATH "${CG_TOOLS}/lib")
include_directories("${CG_TOOLS}/include")
include_directories("${CG_TOOLS}/include/libcxx")
link_directories("${CG_TOOLS}/lib")
string(APPEND CMAKE_CXX_FLAGS " -mv7M3 --code_state=16 --float_support=none -me")
string(APPEND CMAKE_CXX_FLAGS_DEBUG " -g -Ooff --opt_for_speed=1 --symdebug:dwarf_version=3")
string(APPEND CMAKE_CXX_FLAGS_RELEASE " -O3 --opt_for_speed=3")
string(APPEND CMAKE_C_FLAGS " -mv7M3 --code_state=16 --float_support=none -me")
string(APPEND CMAKE_C_FLAGS_DEBUG " -g -Ooff --opt_for_speed=1 --symdebug:dwarf_version=3")
string(APPEND CMAKE_C_FLAGS_RELEASE " -O3 --opt_for_speed=3")
string(APPEND CMAKE_EXE_LINKER_FLAGS "--reread_libs --rom_model -z")
string(APPEND CMAKE_STATIC_LINKER_FLAGS "--rom_model")

