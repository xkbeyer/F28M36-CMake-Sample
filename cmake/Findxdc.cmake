# CMake find_package() Module for XDCTOOLS
#
# Example usage:
#
# find_package(xdc)
#
# If successful the following variables will be defined
# XDC_FOUND
# XDC_EXECUTABLE
# XDC_PKG           The packages of the xdc itself.
# XDCTOOLS_VERSION
# XDCTOOLS_VER_1
# XDCTOOLS_VER_2
# XDCTOOLS_VER_3
# XDCTOOLS_VER_4

project(xdc)
message(STATUS "Config ${PROJECT_NAME}")
if(WIN32)
    set(PLATFORM_SPECIFIC_HINTS c:/ti)
else()
    set(PLATFORM_SPECIFIC_HINTS ~/ti)
endif()

#unset(xdc_EXECUTABLE CACHE)
find_program(xdc_EXECUTABLE
             NAMES xs 
             HINTS ENV XDCTOOLS ENV TI_DIR $ENV{TI_DIR}/.. ${PLATFORM_SPECIFIC_HINTS}
             PATH_SUFFIXES xdctools_3_32_00_06_core xdctools_3_32_02_25_core xdctools_3_50_05_12_core 
             DOC "Path to XDC TOOLS xs executable")
             
if(xdc_EXECUTABLE)
    get_filename_component(XDCCORE "${xdc_EXECUTABLE}" DIRECTORY)
    set(XDC_PKG ${XDCCORE}/packages)
    get_filename_component(_XDCCORE ${XDCCORE} NAME) 
    string(REGEX MATCH "xdctools_([3-9])_([0-9][0-9])_([0-9][0-9])_([0-9][0-9])_core" _xdcversion ${_XDCCORE})    
    set(XDCTOOLS_VERSION "${CMAKE_MATCH_1}.${CMAKE_MATCH_2}.${CMAKE_MATCH_3}.${CMAKE_MATCH_4}")
    set(XDCTOOLS_VER_1 "${CMAKE_MATCH_1}")
    set(XDCTOOLS_VER_2 "${CMAKE_MATCH_2}")
    set(XDCTOOLS_VER_3 "${CMAKE_MATCH_3}")
    set(XDCTOOLS_VER_4 "${CMAKE_MATCH_4}")
endif()

# Handle REQUIRED and QUIET arguments
# this will also set xdc_FOUND to true if xdc_EXECUTABLE exists
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(xdc "Failed to locate xdc executable" xdc_EXECUTABLE)

# Check if XDCTOOLS_JAVA_HOME is set
set(XDCTOOLS_JAVA_HOME $ENV{XDCTOOLS_JAVA_HOME})
if( NOT EXISTS ${XDCTOOLS_JAVA_HOME} )
    # Check if XDC is installed with it's own jre
    find_program(java_EXECUTABLE NAMES javaw HINTS ${XDCCORE}/jre/bin)
    if(java_EXECUTABLE)
        get_filename_component(JAVA_HOME_BIN java_EXECUTABLE DIRECTORY)
        string(REPLACE "bin" "" XDCTOOLS_JAVA_HOME JAVA_HOME_BIN)
    else()
        # See if java 32 bit is installed or in path
        find_package(Java COMPONENTS Runtime)
        if( Java_JAVA_EXECUTABLE )
            get_filename_component(Java_REAL_DIR ${Java_JAVA_EXECUTABLE} REALPATH)
            get_filename_component(Java_BIN_DIR ${Java_REAL_DIR} DIRECTORY)
            if(WIN32)
                execute_process(COMMAND ${Java_JAVA_EXECUTABLE} -version OUTPUT_VARIABLE _output ERROR_VARIABLE _error)
                string(FIND "${_error}" "64-Bit" _position)
                if(_position EQUAL -1)
                    set(XDCTOOLS_JAVA_HOME ${Java_BIN_DIR}/..)
                endif()
            else()
                set(XDCTOOLS_JAVA_HOME ${Java_BIN_DIR}/..)
            endif()
        endif()
    endif()
endif()
if(NOT EXISTS ${XDCTOOLS_JAVA_HOME})
    message(FATAL_ERROR "No 32 bit java runtime found ${XDCTOOLS_JAVA_HOME}")
endif()
