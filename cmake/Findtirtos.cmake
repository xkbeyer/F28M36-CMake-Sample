# CMake find_package() Module for TI RTOS
#
# Example usage:
#
# find_package(tirtos)
#
# If successful the following variables will be defined
# TIRTOS_FOUND
# TIRTOS_DIR the path to the TI RTOS

cmake_minimum_required(VERSION 3.10)
project(tirtos VERSION 2.16.00.08)
message(STATUS "Config ${PROJECT_NAME}")
set(PRODUCTS_2_16_0_8 bios_6_45_01_29 tidrivers_c2000_2_16_00_08 MWare_v202a ndk_2_25_00_09 uia_2_00_05_50)
set(PRODUCTS_2_16_1_14 bios_6_45_02_31 tidrivers_c2000_2_16_01_13 MWare_v202a ndk_2_25_00_09 uia_2_00_05_50)
set(BIOS_VERS bios_6_45_01_29 bios_6_45_02_31)
set(DRIVER_VERS tidrivers_c2000_2_16_00_08 tidrivers_c2000_2_16_01_13)
set(UIA_VERS uia_2_00_05_50)
set(MWARE_VERS MWare_v202a)
set(NDK_VERS ndk_2_25_00_09)

if(WIN32)
set(PLATFORM_SPECIFIC_HINTS c:/ti)
else()
set(PLATFORM_SPECIFIC_HINTS ~/ti)
endif()

# Have to set the CMAKE_FIND_ROOT_PATH otherwise for some reason the find_path doesn't work.
# Even the HINTS or PATHS are set.
list(APPEND CMAKE_FIND_ROOT_PATH $ENV{TI_DIR})

find_path(
            TIRTOS_DIR tirtos.mak 
            HINTS ENV TI_DIR #Need to set this too. W/o it doesn't work.  
            PATH_SUFFIXES "tirtos_c2000_2_16_00_08" "tirtos_c2000_2_16_01_14"
            DOC "Path to TI RTOS installation"
            )

# Handle REQUIRED and QUIET arguments
# this will also set TIRTOS_FOUND to true if TIRTOS path exists
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(TIRTOS
                                  "Failed to locate TI RTOS path"
                                  TIRTOS_DIR)
if(TIRTOS_FOUND)
    set(RTOS_PKG ${TIRTOS_DIR}/packages)
    # XDC Tools 3.50 needs the bios 6.50
    if(XDCTOOLS_VERSION STREQUAL "3.50.05.12")
        find_path(
            BIOS_PKG bios.mak 
            HINTS ${TIRTOS_DIR}/products ENV TI_DIR #Need to set this too. W/o it doesn't work.  
            PATH_SUFFIXES bios_6_50_01_12 
            )
    else()
        find_path(
            BIOS_PKG bios.mak 
            HINTS ${TIRTOS_DIR}/products #Need to set this too. W/o it doesn't work.  
            PATH_SUFFIXES ${BIOS_VERS}
            )
    endif()
    set(BIOS_PKG ${BIOS_PKG}/packages)
    find_path(
            TIDRIVERS_PKG drivers.mak 
            HINTS ${TIRTOS_DIR}/products #Need to set this too. W/o it doesn't work.  
            PATH_SUFFIXES ${DRIVER_VERS}
            )
    set(TIDRIVERS_PKG ${TIDRIVERS_PKG}/packages)

    find_path(
            UIA_PKG uia.mak 
            HINTS ${TIRTOS_DIR}/products #Need to set this too. W/o it doesn't work.  
            PATH_SUFFIXES ${UIA_VERS}
            )
    set(UIA_PKG ${UIA_PKG}/packages)

    find_path(
            MWARE_PKG driverlib 
            HINTS ${TIRTOS_DIR}/products #Need to set this too. W/o it doesn't work.  
            PATH_SUFFIXES ${MWARE_VERS}
            )

    find_path(
            NDK_PKG ndk.mak 
            HINTS ${TIRTOS_DIR}/products #Need to set this too. W/o it doesn't work.  
            PATH_SUFFIXES ${NDK_VERS}
            )
    set(NDK_PKG ${NDK_PKG}/packages)
endif()
