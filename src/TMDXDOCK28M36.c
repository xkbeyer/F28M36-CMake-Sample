/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== TMDXDOCK28M36.c ========
 *  This file is responsible for setting up the board specific items for the
 *  TMDXDOCK28M36 board.
 */

#include <stdint.h>
#include <stdbool.h>

#include <xdc/std.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/System.h>
#include <ti/sysbios/family/arm/m3/Hwi.h>

#include <inc/hw_gpio.h>
#include <inc/hw_ints.h>
#include <inc/hw_memmap.h>
#include <inc/hw_sysctl.h>
#include <inc/hw_types.h>

#include <driverlib/gpio.h>
#include <driverlib/i2c.h>
#include <driverlib/ssi.h>
#include <driverlib/sysctl.h>
#include <driverlib/uart.h>
#include <driverlib/udma.h>

#include "TMDXDOCK28M36.h"

/*
 *  =============================== DMA ===============================
 */
#pragma DATA_SECTION(dmaControlTable, ".dma");
#pragma DATA_ALIGN(dmaControlTable, 1024)
static tDMAControlTable dmaControlTable[32];
static bool dmaInitialized = false;

/* Hwi_Struct used in the initDMA Hwi_construct call */
static Hwi_Struct dmaHwiStruct;

/*
 *  ======== dmaErrorHwi ========
 */
static Void dmaErrorHwi(UArg arg)
{
    System_printf("DMA error code: %d\n", uDMAErrorStatusGet());
    uDMAErrorStatusClear();
    System_abort("DMA error!!");
}

/*
 *  ======== TMDXDOCK28M36_initDMA ========
 */
void TMDXDOCK28M36_initDMA(void)
{
    Error_Block eb;
    Hwi_Params  hwiParams;

    if (!dmaInitialized) {
        Error_init(&eb);
        Hwi_Params_init(&hwiParams);
        Hwi_construct(&(dmaHwiStruct), INT_UDMAERR, dmaErrorHwi,
                      &hwiParams, &eb);
        if (Error_check(&eb)) {
            System_abort("Couldn't construct DMA error hwi");
        }

        SysCtlPeripheralEnable(SYSCTL_PERIPH_UDMA);
        uDMAEnable();
        uDMAControlBaseSet(dmaControlTable);

        dmaInitialized = true;
    }
}

/*
 *  =============================== EMAC ===============================
 */
/* Place into subsections to allow the TI linker to remove items properly */
#pragma DATA_SECTION(EMAC_config, ".const:EMAC_config")
#pragma DATA_SECTION(emacHWAttrs, ".const:emacHWAttrs")

#include <ti/drivers/EMAC.h>
#include <ti/drivers/emac/EMACTiva.h>

EMACTiva_Object emacObjects[TMDXDOCK28M36_EMACCOUNT];

/*
 *  EMAC configuration structure
 *  Set user/company specific MAC octates. The following sets the address
 *  to ff-ff-ff-ff-ff-ff. Users need to change this to match the label on
 *  their boards.
 */
#ifndef MACADDRESS
#define MACADDRESS 0xFF,0xFF,0xFF,0xFF,0xFF,0xFF
#endif
unsigned char macAddress[6] = {MACADDRESS};

/*
 *  Required by the Networking Stack (NDK). This array must be NULL terminated.
 *  This can be removed if NDK is not used.
 *  Double curly braces are needed to avoid GCC bug #944572
 *  https://bugs.launchpad.net/gcc-linaro/+bug/944572
 */
#pragma DATA_SECTION(NIMUDeviceTable, ".data:NIMUDeviceTable")
NIMU_DEVICE_TABLE_ENTRY  NIMUDeviceTable[2] = {{.init = EMACTiva_NIMUInit}, {NULL}};

const EMACTiva_HWAttrs emacHWAttrs[TMDXDOCK28M36_EMACCOUNT] = {
    {
        .intNum = INT_ETH,
        .intPriority = (~0),
        .macAddress = macAddress
    }
};

const EMAC_Config EMAC_config[] = {
    {
        .fxnTablePtr = &EMACTiva_fxnTable,
        .object = &emacObjects[0],
        .hwAttrs = &emacHWAttrs[0]
    },
    {NULL, NULL, NULL}
};

/*
 *  ======== TMDXDOCK28M36_initEMAC ========
 */
void TMDXDOCK28M36_initEMAC(void)
{
    GPIODirModeSet(GPIO_PORTK_BASE,
                   GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_7,
                   GPIO_DIR_MODE_HW);
	GPIOPadConfigSet(GPIO_PORTK_BASE,
                     GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_7,
                     GPIO_PIN_TYPE_STD);
	GPIOPinConfigure(GPIO_PK4_MIITXEN);
	GPIOPinConfigure(GPIO_PK5_MIITXCK);
	GPIOPinConfigure(GPIO_PK7_MIICRS);

    GPIODirModeSet(GPIO_PORTL_BASE,
                   GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3 |
                   GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7,
                   GPIO_DIR_MODE_HW);
	GPIOPadConfigSet(GPIO_PORTL_BASE,
                     GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3 |
                     GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7,
                     GPIO_PIN_TYPE_STD);
	GPIOPinConfigure(GPIO_PL0_MIIRXD3);
	GPIOPinConfigure(GPIO_PL1_MIIRXD2);
	GPIOPinConfigure(GPIO_PL2_MIIRXD1);
	GPIOPinConfigure(GPIO_PL3_MIIRXD0);
	GPIOPinConfigure(GPIO_PL4_MIICOL);
	GPIOPinConfigure(GPIO_PL5_MIIPHYRSTN);
	GPIOPinConfigure(GPIO_PL6_MIIPHYINTRN);
	GPIOPinConfigure(GPIO_PL7_MIIMDC);

	GPIODirModeSet(GPIO_PORTM_BASE,
                   GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3 |
                   GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7,
                   GPIO_DIR_MODE_HW);
	GPIOPadConfigSet(GPIO_PORTM_BASE,
                     GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3 |
                     GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7,
                     GPIO_PIN_TYPE_STD);
	GPIOPinConfigure(GPIO_PM0_MIIMDIO);
	GPIOPinConfigure(GPIO_PM1_MIITXD3);
	GPIOPinConfigure(GPIO_PM2_MIITXD2);
	GPIOPinConfigure(GPIO_PM3_MIITXD1);
	GPIOPinConfigure(GPIO_PM4_MIITXD0);
	GPIOPinConfigure(GPIO_PM5_MIIRXDV);
	GPIOPinConfigure(GPIO_PM6_MIIRXER);
	GPIOPinConfigure(GPIO_PM7_MIIRXCK);

    /* Enable and Reset the Ethernet Controller. */
    SysCtlPeripheralEnable(SYSCTL_PERIPH_ETH);
    SysCtlPeripheralReset(SYSCTL_PERIPH_ETH);

    if (macAddress[0] == 0xff && macAddress[1] == 0xff &&
        macAddress[2] == 0xff && macAddress[3] == 0xff &&
        macAddress[4] == 0xff && macAddress[5] == 0xff) {
        System_abort("Change the macAddress variable to match your board's MAC sticker");
    }

    /* Once EMAC_init is called, EMAC_config cannot be changed */
    EMAC_init();
}

/*
 *  =============================== General ===============================
 */
/*
 *  ======== TMDXDOCK28M36_initGeneral ========
 */
void TMDXDOCK28M36_initGeneral(void)
{
    /* Disable Protection */
    HWREG(SYSCTL_MWRALLOW) =  0xA5A5A5A5;

    /* Enable clock supply for the following peripherals */
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOG);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOH);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOJ);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOK);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOL);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOM);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPION);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOP);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOQ);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOR);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOS);

    /* Disable clock supply for the watchdog modules */
    SysCtlPeripheralDisable(SYSCTL_PERIPH_WDOG1);
    SysCtlPeripheralDisable(SYSCTL_PERIPH_WDOG0);
}

/*
 *  =============================== GPIO ===============================
 */
/* Place into subsections to allow the TI linker to remove items properly */
#pragma DATA_SECTION(GPIOTiva_config, ".const:GPIOTiva_config")

#include <ti/drivers/GPIO.h>
#include <ti/drivers/gpio/GPIOTiva.h>

/*
 * Array of Pin configurations
 * NOTE: The order of the pin configurations must coincide with what was
 *       defined in TMDXDOCK28M36.h
 * NOTE: Pins not used for interrupts should be placed at the end of the
 *       array.  Callback entries can be omitted from callbacks array to
 *       reduce memory usage.
 */
GPIO_PinConfig gpioPinConfigs[] = {
    /* Input pins */
    /* TMDXDOCK28M36_BUTTON */
    GPIOTiva_PB_4 | GPIO_CFG_IN_PU | GPIO_CFG_IN_INT_RISING,

    /* Output pins */
    /* TMDXDOCK28M36_D1 */
    GPIOTiva_PE_7 | GPIO_CFG_OUT_STD | GPIO_CFG_OUT_STR_HIGH | GPIO_CFG_OUT_LOW,
    /* TMDXDOCK28M36_D2 */
    GPIOTiva_PF_2 | GPIO_CFG_OUT_STD | GPIO_CFG_OUT_STR_HIGH | GPIO_CFG_OUT_LOW,
};

/*
 * Array of callback function pointers
 * NOTE: The order of the pin configurations must coincide with what was
 *       defined in TMDXDOCK28M36.h
 * NOTE: Pins not used for interrupts can be omitted from callbacks array to
 *       reduce memory usage (if placed at end of gpioPinConfigs array).
 */
GPIO_CallbackFxn gpioCallbackFunctions[] = {
    NULL   /* TMDXDOCK28M36_BUTTON */
};

/* The device-specific GPIO_config structure */
const GPIOTiva_Config GPIOTiva_config = {
    .pinConfigs = (GPIO_PinConfig *)gpioPinConfigs,
    .callbacks = (GPIO_CallbackFxn *)gpioCallbackFunctions,
    .numberOfPinConfigs = sizeof(gpioPinConfigs)/sizeof(GPIO_PinConfig),
    .numberOfCallbacks = sizeof(gpioCallbackFunctions)/sizeof(GPIO_CallbackFxn),
    .intPriority = (~0)
};

/*
 *  ======== TMDXDOCK28M36_initGPIO ========
 */
void TMDXDOCK28M36_initGPIO(void)
{
    /* Initialize peripheral and pins */
    GPIO_init();
}

/*
 *  =============================== I2C ===============================
 */
/* Place into subsections to allow the TI linker to remove items properly */
#pragma DATA_SECTION(I2C_config, ".const:I2C_config")
#pragma DATA_SECTION(i2cTivaHWAttrs, ".const:i2cTivaHWAttrs")

#include <ti/drivers/I2C.h>
#include <ti/drivers/i2c/I2CTiva.h>

I2CTiva_Object i2cTivaObjects[TMDXDOCK28M36_I2CCOUNT];

const I2CTiva_HWAttrs i2cTivaHWAttrs[TMDXDOCK28M36_I2CCOUNT] = {
    {
        .baseAddr = I2C0_MASTER_BASE,
        .intNum = INT_I2C0,
        .intPriority = (~0)
    }
};

const I2C_Config I2C_config[] = {
    {
        .fxnTablePtr = &I2CTiva_fxnTable,
        .object = &i2cTivaObjects[0],
        .hwAttrs = &i2cTivaHWAttrs[0]
    },
    {NULL, NULL, NULL}
};

/*
 *  ======== TMDXDOCK28M36_initI2C ========
 */
void TMDXDOCK28M36_initI2C(void)
{
    /* I2C0 Init */
    /* Enable the peripheral */
    SysCtlPeripheralEnable(SYSCTL_PERIPH_I2C0);

    /* Configure the appropriate pins to be I2C instead of GPIO. */
    GPIOPinConfigure(GPIO_PF0_I2C0SDA); /* GPIO32--85 on dock*/
    GPIOPinConfigure(GPIO_PF1_I2C0SCL); /* GPIO33--87 on dock */
    GPIOPinTypeI2C(GPIO_PORTF_BASE, GPIO_PIN_0 | GPIO_PIN_1);

    I2C_init();
}

/*
 *  =============================== SDSPI ===============================
 */
/* Place into subsections to allow the TI linker to remove items properly */
#pragma DATA_SECTION(SDSPI_config, ".const:SDSPI_config")
#pragma DATA_SECTION(sdspiTivaHWattrs, ".const:sdspiTivaHWattrs")

#include <ti/drivers/SDSPI.h>
#include <ti/drivers/sdspi/SDSPITiva.h>

SDSPITiva_Object sdspiTivaObjects[TMDXDOCK28M36_SDSPICOUNT];

const SDSPITiva_HWAttrs sdspiTivaHWattrs[TMDXDOCK28M36_SDSPICOUNT] = {
    {
        .baseAddr = SSI3_BASE,

        .portSCK = GPIO_PORTR_BASE,
        .pinSCK = GPIO_PIN_2,
        .portMISO = GPIO_PORTR_BASE,
        .pinMISO = GPIO_PIN_1,
        .portMOSI = GPIO_PORTR_BASE,
        .pinMOSI = GPIO_PIN_0,
        .portCS = GPIO_PORTR_BASE,
        .pinCS = GPIO_PIN_3
    }
};

const SDSPI_Config SDSPI_config[] = {
    {
        .fxnTablePtr = &SDSPITiva_fxnTable,
        .object = &sdspiTivaObjects[0],
        .hwAttrs = &sdspiTivaHWattrs[0]
    },
    {NULL, NULL, NULL}
};

/*
 *  ======== TMDXDOCK28M36_initSDSPI ========
 */
void TMDXDOCK28M36_initSDSPI(void)
{
    /* Enable the peripherals used by the SD Card */
    SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI3);

    /* Configure SPI Peripheral pin muxing */
    GPIOPinConfigure(GPIO_PR2_SSI3CLK);
    GPIOPinConfigure(GPIO_PR0_SSI3TX);
    GPIOPinConfigure(GPIO_PR1_SSI3RX);

    /* Configure pad settings */
    GPIOPadConfigSet(GPIO_PORTR_BASE,
            GPIO_PIN_2 | GPIO_PIN_1 | GPIO_PIN_0,
            GPIO_PIN_TYPE_STD_WPU);

    GPIOPadConfigSet(GPIO_PORTR_BASE, GPIO_PIN_3, GPIO_PIN_TYPE_STD_WPU);

    SDSPI_init();
}

/*
 *  =============================== SPI ===============================
 */
/* Place into subsections to allow the TI linker to remove items properly */
#pragma DATA_SECTION(SPI_config, ".const:SPI_config")
#pragma DATA_SECTION(spiTivaDMAHWAttrs, ".const:spiTivaDMAHWAttrs")

#include <ti/drivers/SPI.h>
#include <ti/drivers/spi/SPITivaDMA.h>

SPITivaDMA_Object spiTivaDMAObjects[TMDXDOCK28M36_SPICOUNT];

#pragma DATA_SECTION(spiTivaDMAscratchBuf, ".dma");
#pragma DATA_ALIGN(spiTivaDMAscratchBuf, 32)
uint32_t spiTivaDMAscratchBuf[TMDXDOCK28M36_SPICOUNT];

const SPITivaDMA_HWAttrs spiTivaDMAHWAttrs[TMDXDOCK28M36_SPICOUNT] = {
    {
        .baseAddr = SSI0_BASE,
        .intNum = INT_SSI0,
        .intPriority = (~0),
        .scratchBufPtr = &spiTivaDMAscratchBuf[0],
        .defaultTxBufValue = 0,
        .rxChannelIndex = UDMA_CHANNEL_SSI0RX,
        .txChannelIndex = UDMA_CHANNEL_SSI0TX,
        .channelMappingFxn = uDMAChannel8_15SelectDefault,
        .rxChannelMappingFxnArg = UDMA_CHAN10_DEF_SSI0RX_M,
        .txChannelMappingFxnArg = UDMA_CHAN11_DEF_SSI0TX_M
    },
    {
        .baseAddr = SSI1_BASE,
        .intNum = INT_SSI1,
        .intPriority = (~0),
        .scratchBufPtr = &spiTivaDMAscratchBuf[1],
        .defaultTxBufValue = 0,
        .rxChannelIndex = UDMA_CHANNEL_SSI1RX,
        .txChannelIndex = UDMA_CHANNEL_SSI1TX,
        .channelMappingFxn = uDMAChannel24_31SelectDefault,
        .rxChannelMappingFxnArg = UDMA_CHAN24_DEF_SSI1RX_M,
        .txChannelMappingFxnArg = UDMA_CHAN25_DEF_SSI1TX_M
    },
    {
        .baseAddr = SSI2_BASE,
        .intNum = INT_SSI2,
        .intPriority = (~0),
        .scratchBufPtr = &spiTivaDMAscratchBuf[2],
        .defaultTxBufValue = 0,
        .rxChannelIndex = UDMA_THRD_CHANNEL_SSI2RX,
        .txChannelIndex = UDMA_THRD_CHANNEL_SSI2TX,
        .channelMappingFxn = uDMAChannel8_15SelectAltMapping,
        .rxChannelMappingFxnArg = UDMA_CHAN12_THRD_SSI2RX,
        .txChannelMappingFxnArg = UDMA_CHAN13_THRD_SSI2TX
    },
    {
        .baseAddr = SSI3_BASE,
        .intNum = INT_SSI3,
        .intPriority = (~0),
        .scratchBufPtr = &spiTivaDMAscratchBuf[3],
        .defaultTxBufValue = 0,
        .rxChannelIndex = UDMA_THRD_CHANNEL_SSI3RX,
        .txChannelIndex = UDMA_THRD_CHANNEL_SSI3TX,
        .channelMappingFxn = uDMAChannel8_15SelectAltMapping,
        .rxChannelMappingFxnArg = UDMA_CHAN14_THRD_SSI3RX,
        .txChannelMappingFxnArg = UDMA_CHAN15_THRD_SSI3TX
    }
};

const SPI_Config SPI_config[] = {
    {
        .fxnTablePtr = &SPITivaDMA_fxnTable,
        .object = &spiTivaDMAObjects[0],
        .hwAttrs = &spiTivaDMAHWAttrs[0]
    },
    {
        .fxnTablePtr = &SPITivaDMA_fxnTable,
        .object = &spiTivaDMAObjects[1],
        .hwAttrs = &spiTivaDMAHWAttrs[1]
    },
    {
        .fxnTablePtr = &SPITivaDMA_fxnTable,
        .object = &spiTivaDMAObjects[2],
        .hwAttrs = &spiTivaDMAHWAttrs[2]
    },
    {
        .fxnTablePtr = &SPITivaDMA_fxnTable,
        .object = &spiTivaDMAObjects[3],
        .hwAttrs = &spiTivaDMAHWAttrs[3]
    },
    {NULL, NULL, NULL},
};

/*
 *  ======== TMDXDOCK28M36_initSPI ========
 */
void TMDXDOCK28M36_initSPI(void)
{
    /* SSI0 */
    SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI0);

    GPIOPinConfigure(GPIO_PD2_SSI0CLK);
    GPIOPinConfigure(GPIO_PD3_SSI0FSS);
    GPIOPinConfigure(GPIO_PD1_SSI0RX);
    GPIOPinConfigure(GPIO_PD0_SSI0TX);

    GPIOPinTypeSSI(GPIO_PORTD_BASE, GPIO_PIN_0 | GPIO_PIN_1 |
                                    GPIO_PIN_2 | GPIO_PIN_3);

    /* SSI1 */
    SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI1);

    GPIOPinConfigure(GPIO_PE0_SSI1CLK);
    GPIOPinConfigure(GPIO_PE1_SSI1FSS);
    GPIOPinConfigure(GPIO_PE2_SSI1RX);
    GPIOPinConfigure(GPIO_PE3_SSI1TX);

    GPIOPinTypeSSI(GPIO_PORTE_BASE, GPIO_PIN_0 | GPIO_PIN_1 |
                                    GPIO_PIN_2 | GPIO_PIN_3);
    TMDXDOCK28M36_initDMA();
    SPI_init();
}

/*
 *  =============================== UART ===============================
 */
/* Place into subsections to allow the TI linker to remove items properly */
#pragma DATA_SECTION(UART_config, ".const:UART_config")
#pragma DATA_SECTION(uartTivaHWAttrs, ".const:uartTivaHWAttrs")

#include <ti/drivers/UART.h>
#include <ti/drivers/uart/UARTTiva.h>

UARTTiva_Object uartTivaObjects[TMDXDOCK28M36_UARTCOUNT];
unsigned char uartTivaRingBuffer[TMDXDOCK28M36_UARTCOUNT][32];

/* UART configuration structure */
const UARTTiva_HWAttrs uartTivaHWAttrs[TMDXDOCK28M36_UARTCOUNT] = {
    {
        .baseAddr = UART0_BASE,
        .intNum = INT_UART0,
        .intPriority = (~0),
        .flowControl = UART_FLOWCONTROL_NONE,
        .ringBufPtr  = uartTivaRingBuffer[0],
        .ringBufSize = sizeof(uartTivaRingBuffer[0])
    }
};

const UART_Config UART_config[] = {
    {
        .fxnTablePtr = &UARTTiva_fxnTable,
        .object = &uartTivaObjects[0],
        .hwAttrs = &uartTivaHWAttrs[0]
    },
    {NULL, NULL, NULL}
};

/*
 *  ======== TMDXDOCK28M36_initUART ========
 */
void TMDXDOCK28M36_initUART(void)
{
    /* Enable and configure the peripherals used by the uart. */
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
    GPIOPinTypeUART(GPIO_PORTE_BASE, GPIO_PIN_4 | GPIO_PIN_5);
    GPIOPinConfigure(GPIO_PE4_U0RX);
    GPIOPinConfigure(GPIO_PE5_U0TX);
    GPIOPadConfigSet(GPIO_PORTE_BASE, GPIO_PIN_4 | GPIO_PIN_5, GPIO_PIN_TYPE_STD_WPU);

    /* Initialize the UART driver */
    UART_init();
}

/*
 *  =============================== USB ===============================
 */
/*
 *  ======== TMDXDOCK28M36_initUSB ========
 *  This function just turns on the USB
 */
void TMDXDOCK28M36_initUSB(TMDXDOCK28M36_USBMode usbMode)
{
    /*
     * Setup USB clock tree for 60MHz
     * 20MHz * 12 / 4 = 60
     */
    SysCtlUSBPLLConfigSet(
            (SYSCTL_UPLLIMULT_M & 12) | SYSCTL_UPLLCLKSRC_X1 | SYSCTL_UPLLEN);

    /* Setup pins for USB operation */
    GPIOPinTypeUSBAnalog(GPIO_PORTF_BASE, GPIO_PIN_6);
    GPIOPinTypeUSBAnalog(
            GPIO_PORTG_BASE, GPIO_PIN_2 | GPIO_PIN_5 | GPIO_PIN_6);

    SysCtlPeripheralEnable(SYSCTL_PERIPH_USB0);

    /* Additional configurations for Host mode */
    if (usbMode == TMDXDOCK28M36_USBHOST) {
        /* Configure the pins needed */
        GPIOPinConfigure(GPIO_PN6_USB0EPEN);
        GPIOPinConfigure(GPIO_PN7_USB0PFLT);
        GPIOPinTypeUSBDigital(GPIO_PORTN_BASE, GPIO_PIN_6 | GPIO_PIN_7);
    }
}

/*
 *  =============================== USBMSCHFatFs ===============================
 */
/* Place into subsections to allow the TI linker to remove items properly */
#pragma DATA_SECTION(USBMSCHFatFs_config, ".const:USBMSCHFatFs_config")
#pragma DATA_SECTION(usbmschfatfstivaHWAttrs, ".const:usbmschfatfstivaHWAttrs")

#include <ti/drivers/USBMSCHFatFs.h>
#include <ti/drivers/usbmschfatfs/USBMSCHFatFsTiva.h>

USBMSCHFatFsTiva_Object usbmschfatfstivaObjects[TMDXDOCK28M36_USBMSCHFatFsCOUNT];

const USBMSCHFatFsTiva_HWAttrs usbmschfatfstivaHWAttrs[TMDXDOCK28M36_USBMSCHFatFsCOUNT] = {
    {
        .intNum = INT_USB0,
        .intPriority = (~0)
    }
};

const USBMSCHFatFs_Config USBMSCHFatFs_config[] = {
    {
        .fxnTablePtr = &USBMSCHFatFsTiva_fxnTable,
        .object = &usbmschfatfstivaObjects[0],
        .hwAttrs = &usbmschfatfstivaHWAttrs[0]
    },
    {NULL, NULL, NULL}
};

/*
 *  ======== TMDXDOCK28M36_initUSBMSCHFatFs ========
 */
void TMDXDOCK28M36_initUSBMSCHFatFs(void)
{
    /* Call the USB initialization function for the USB Reference modules */
    TMDXDOCK28M36_initUSB(TMDXDOCK28M36_USBHOST);
    USBMSCHFatFs_init();
}

/*
 *  =============================== Watchdog ===============================
 */
/* Place into subsections to allow the TI linker to remove items properly */
#pragma DATA_SECTION(Watchdog_config, ".const:Watchdog_config")
#pragma DATA_SECTION(watchdogTivaHWAttrs, ".const:watchdogTivaHWAttrs")

#include <ti/drivers/Watchdog.h>
#include <ti/drivers/watchdog/WatchdogTiva.h>

WatchdogTiva_Object watchdogTivaObjects[TMDXDOCK28M36_WATCHDOGCOUNT];

const WatchdogTiva_HWAttrs watchdogTivaHWAttrs[TMDXDOCK28M36_WATCHDOGCOUNT] = {
    {
        .baseAddr = WATCHDOG0_BASE,
        .intNum = INT_WATCHDOG,
        .intPriority = (~0),
        .reloadValue = 75000000 // 1 second period at default CPU clock freq
    },
};

const Watchdog_Config Watchdog_config[] = {
    {
        .fxnTablePtr = &WatchdogTiva_fxnTable,
        .object = &watchdogTivaObjects[0],
        .hwAttrs = &watchdogTivaHWAttrs[0]
    },
    {NULL, NULL, NULL},
};

/*
 *  ======== TMDXDOCK28M36_initWatchdog ========
 */
void TMDXDOCK28M36_initWatchdog(void)
{
    /* Enable peripherals used by Watchdog */
    SysCtlPeripheralEnable(SYSCTL_PERIPH_WDOG0);

    Watchdog_init();
}
