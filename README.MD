# CMake Support TI-RTOS FM28M36 Cortex M3
An example CMakeLists.txt file to use the TI compiler and TI-RTOS in a project.
The current one is taken from the tirtos_c2000_2_16_00_08 sample (bigtime).

## Assumptions
- Environment variable TI_DIR is set
- TI-RTOS, XDC and TI compiler are installed under TI_DIR.
- Environment variable CG_TOOLS points to the TI compiler. 
- On Windows the environment variable XDCTOOLS_JAVA_HOME points to the JRE.

## Toolchain file
The Toolchain-ti-f28m36-arm.cmake sets up the compiler, linker, ar etc to be used by the project.
The base C/CXX, Linker flags are set for the F28M36 ARM controller.

## XDC
To build the TI-RTOS the XDC tools are used.
### Windows 
Under Windows a 32 bit Java runtime must be used.
Therefor the XDCTOOLS_JAVA_HOME must point to the 32 bit Java JRE path like c:\Program Files (x86)\Java\jre1.8.0_172.

## Dependencies
- tirtos_c2000_2_16_00_08, 2_16_01_14
- xdctools_3_50_05_12_core, 3_32_00_06, 3_32_02_25
- ti-cgt-arm_18.1.1.LTS

## Usage
```
mkdir -p build
cd build
cmake -G "Unix Makefiles" -DCMAKE_TOOLCHAIN_FILE=../cmake/Toolchain-ti-f28m36-arm.cmake -DCMAKE_BUILD_TYPE=Debug .. 
make
```
